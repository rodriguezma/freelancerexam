package com.exam.movlancer.ui.details

import com.exam.movlancer.R
import com.exam.movlancer.data.repository.NetworkState
import com.exam.movlancer.data.vo.MovieDetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.exam.movlancer.data.api.*
import kotlinx.android.synthetic.main.activity_movie_details.*
import java.text.NumberFormat
import java.util.Locale

class MovieDetailsActivity : AppCompatActivity() {

    private lateinit var detailsViewModel: MovieDetailsViewModel
    private lateinit var movieRepository: MovieDetailsRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        val movieId: Int = intent.getIntExtra("id",1)

        val apiService : TheMovieDBInterface = TheMovieDBClient.getClient()
        movieRepository = MovieDetailsRepository(apiService)

        detailsViewModel = getViewModel(movieId)

        detailsViewModel.movieDetailsDetails.observe(this, Observer {
            bindUI(it)
        })

        detailsViewModel.networkState.observe(this, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE

        })

    }

    private fun bindUI( it: MovieDetails){
        val MINUTES = " minutes"
        val sb = StringBuilder()
        sb.append(it.runtime.toString()).append(MINUTES)

        movie_title.text = it.title
        movie_tagline.text = it.tagline
        movie_release_date.text = it.releaseDate
        movie_rating.text = it.rating.toString()
        movie_runtime.text = sb.toString()
        movie_overview.text = it.overview

        val formatCurrency = NumberFormat.getCurrencyInstance(Locale.US)
        movie_budget.text = formatCurrency.format(it.budget)
        movie_revenue.text = formatCurrency.format(it.revenue)

        val moviePosterURL = DETAIL_POSTER_BASE_URL + it.backdropPath


        Glide.with(this)
            .load(moviePosterURL)
            .transition(DrawableTransitionOptions.with(MoviePosterAnimation()))
            .into(iv_movie_poster)
    }


    private fun getViewModel(movieId:Int): MovieDetailsViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return MovieDetailsViewModel(movieRepository,movieId) as T
            }
        })[MovieDetailsViewModel::class.java]
    }
}
