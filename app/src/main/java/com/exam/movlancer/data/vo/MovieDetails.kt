package com.exam.movlancer.data.vo

import com.google.gson.annotations.SerializedName

data class MovieDetails(
    val id: Int,
    val budget: Int,
    val video: Boolean,
    val revenue: Long,
    val runtime: Int,
    val title: String,
    val status: String,
    val tagline: String,
    val overview: String,
    val popularity: Double,

    @SerializedName("poster_path")
    val posterPath: String,

    @SerializedName("backdrop_path")
    val backdropPath: String,

    @SerializedName("release_date")
    val releaseDate: String,

    @SerializedName("vote_average")
    val rating: Double
)