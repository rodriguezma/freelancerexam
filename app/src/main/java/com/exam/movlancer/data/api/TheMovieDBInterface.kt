package com.exam.movlancer.data.api

import com.exam.movlancer.data.vo.MovieDetails
import com.exam.movlancer.data.vo.MovieResponse

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDBInterface {

    @GET(POPULAR_MOVIE)
    fun getPopularMovie(@Query("page") page: Int): Single<MovieResponse>

    @GET(MOVIE_ID)
    fun getMovieDetails(@Path("movie_id") id: Int): Single<MovieDetails>

}